# Angular Internship Survival Kit

### Library Angular:
<hr>

**Bootstrap for Angular:** https://ng-bootstrap.github.io/#/home

**Semantic UI for Angular:** https://edcarroll.github.io/ng2-semantic-ui

**Onsen UI for Angular:** https://onsen.io/angular2/

**Kendo UI for Angular:** https://www.telerik.com/kendo-angular-ui

**NG-ZORRO:** https://ng.ant.design/docs/introduce/en

**Angular Material (Official library of Angular):** https://material.angular.io/

**AngularFire 2 (The official Angular library for Firebase.):** https://github.com/angular/angularfire2

**PrimeNG (Angular library with a lot of theme and component):** https://www.primefaces.org/

**Bit (Library of components):** https://bit.dev/components

**Laravel & Angular (PHP package for implement Angular on Laravel):** https://laravel-angular.io/

**NGX Translate (Angular library for translate)** http://www.ngx-translate.com/

**NGX Restangular (Angular library for RESTful API)** https://ngx-restangular.com/

### Tools: 
<hr>

**Angular Augury (Dev Tools extension for debugging Angular applications):** https://augury.rangle.io/

**Compodoc (Generate your Angular project documentation):** https://compodoc.app/

**Generator-Angular2-Library (Generate Angular Library):** https://github.com/jvandemo/generator-angular2-library

**Angular Console (Interface graphique pour les projets Angular/Ionic):** https://angularconsole.com/

**List of Free Angular Template:** https://themeselection.com/40-best-free-premium-angular-admin-template/

<hr>

> "Less Is More"  - Mies van der Rohe